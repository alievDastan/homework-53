import React from "react";

const Form = props => {
    return (
        <form onSubmit={props.onSubmit}>
            <input
                type="text"
                value={props.value}
                onChange={props.onChange}
                placeholder="Write a task"
            />
            <button className="AddNewTask" type="submit">Add</button>
        </form>
    );
}

export default Form;
