import React from "react";
import {nanoid} from "nanoid";

const ToDoList = props => {
    const [id] = React.useState(nanoid);
    const { name, done } = props.todo;
    return (
        <li key={id} style={{ textDecoration: done ? "line-through" : "" }}>
            {name}
            <div>
                {!done ? <button onClick={props.completed}>&#10004;</button> : ""}
                <button onClick={props.remove}>&#10008;</button>
            </div>
        </li>
    );
}

export default ToDoList;
