import React, { useState, useEffect } from "react";
import './App.css';

import ToDoList from "./ToDoList";
import Form from "./Form";

const App = () => {
    const [task, setTask] = useState([
        { name: "Buy milk", done: false, id: "t1" },
        { name: "Go to a market", done: false, id: "t2" },
        { name: "Get up 7 o'clock", done: true, id: "t3" }
    ]);

    const [inputValue, setInputValue] = useState("");

    useEffect(() => {
        let count = 0;
        task.map(todo => (!todo.done ? count++ : null));
        document.title = `${count} task${count > 1 ? "s" : ""} todo`;
    });

    const submit = e => {
        e.preventDefault();
        if (inputValue === "") return alert("Task name is required");

        const newArr = task.slice();
        newArr.splice(0, 0, { name: inputValue, done: false });
        setTask(newArr);
        setInputValue("");
    };

    const bntClick = ({ type, index }) => {
        const newArr = task.slice();
        if (type === "remove") newArr.splice(index, 1);
        else if (type === "completed") newArr[index].done = true;

        return setTask(newArr);
    };

    return (
        <>
            <Form
                onSubmit={submit}
                value={inputValue}
                onChange={e => setInputValue(e.target.value)}
            />
            <ul>
                {task.map((todo, index) => (
                    <ToDoList
                        key={todo.id}
                        todo={todo}
                        remove={() => bntClick({ type: "remove", index })}
                        completed={() => bntClick({ type: "completed", index })}
                    />
                ))}
            </ul>
        </>
    );
}

export default App;
